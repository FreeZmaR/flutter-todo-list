import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:todo/src/State/Models/AppState.dart';
import 'package:todo/src/State/Reducers/appReducer.dart';

import 'src/app.dart';

void main() => runApp(App(
      store: Store<AppState>(
        appReducer,
        initialState: AppState(),
      ),
    ));
