import 'package:todo/src/Core/Route/BaseRoute.dart';
import 'package:todo/src/Core/Route/GeneratorRoute.dart';
import 'package:todo/src/Routes/Note/NoteRoute.dart';
import 'Home/HomeRoute.dart';

class AppRoutes extends GeneratorRoute {
 List<BaseRoute> routes = <BaseRoute>[
   HomeRoute(),
   NoteRoute(),
 ];
}
