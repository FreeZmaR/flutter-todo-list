import 'package:flutter/material.dart';
import 'package:todo/src/Core/Route/BaseRoute.dart';
import 'package:todo/src/Pages/Note/NotePage.dart';

class NoteRoute extends BaseRoute{
  final Map<String, Widget> routes = <String, Widget> {
    'Note': NotePage(),
  };
}