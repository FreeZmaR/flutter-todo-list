import 'package:flutter/material.dart';
import 'package:todo/src/Core/Route/BaseRoute.dart';
import 'package:todo/src/Pages/Home/HomePage.dart';

class HomeRoute extends BaseRoute {
  final Map<String, Widget> routes = <String, Widget>{
    'Home': HomePage(),
  };

  Widget get homePage => HomePage();
}
