abstract class BaseStateModel {
  Map<String, Object> get props;

  @override
  int get hashCode {
    int result;

    for (dynamic item in this.props?.entries) {
      result ^= item.hashCode;
    }

    return result;
  }

  @override
  bool operator ==(dynamic newState) {
    bool partOne = identical(this, newState);
    bool partTwo = this.runtimeType == newState.runtimeType;

    if (this.props == null) {
      return partOne || partTwo;
    }

    Map<String, Object> newProps =
        newState?.props != null ? newState.props : {};

    for (dynamic item in this.props?.entries) {
      if (!partTwo) {
        break;
      }

      if (!newProps.containsKey(item.key)) {
        partTwo = false;
        break;
      }

      partTwo = newProps[item.key] == item.value;
    }
    return partOne || partTwo;
  }

  @override
  String toString() =>
      "${this.runtimeType}${this.props != null ? this.props.toString() : "{}"}";
}
