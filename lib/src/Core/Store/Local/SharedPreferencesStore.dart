import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesStore {
  static Future<bool> save(String key, dynamic data) async {
    final prefs = await SharedPreferences.getInstance();

    if (data is String) {
      return prefs.setString(key, data);
    }

    if (data is bool) {
      return prefs.setBool(key, data);
    }

    if (data is int) {
      return prefs.setInt(key, data);
    }

    if (data is double) {
      return prefs.setDouble(key, data);
    }

    if (data is List<String>) {
      return prefs.setStringList(key, data);
    }

    return prefs.setString(key, _serialize(data));
  }

  static Future<dynamic> get(String key, {bool decode = false}) async {
    final prefs = await SharedPreferences.getInstance();

    if (decode) {
      return _deserialize(prefs.get(key));
    }

    return prefs.get(key) ?? null;
  }

  static Future<bool> delete(String key) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.remove(key);
  }

  static String _serialize(dynamic data) => jsonEncode(data);
  static dynamic _deserialize(String data) => jsonDecode(data);
}
