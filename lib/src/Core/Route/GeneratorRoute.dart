import 'package:flutter/material.dart';
import 'package:todo/src/Core/Route/BaseRoute.dart';

abstract class GeneratorRoute {
  List<BaseRoute> routes;

  Map<String, WidgetBuilder> get routing => <String, WidgetBuilder> {
    ...{ for (var route in routes) ...route.getRoutes},
  };
}
