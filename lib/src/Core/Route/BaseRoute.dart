import 'package:flutter/material.dart';

abstract class BaseRoute {
  Map<String, Widget> routes;

  Map<String, WidgetBuilder> get getRoutes => <String, WidgetBuilder>{
        for (var route in routes.entries)
          route.key: this._prepareRoute(route.value)
      };

  WidgetBuilder _prepareRoute(Widget route) => (context) => route;
}
