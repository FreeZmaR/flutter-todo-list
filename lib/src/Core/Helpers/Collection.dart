class Collection {
  static dynamic getDefault(Map map, Object key, {Object defaultValue}) {
    if ([map, key].contains(null)) return null;
    return map.containsKey(key) ? map[key] ?? defaultValue : defaultValue;
  }
}
