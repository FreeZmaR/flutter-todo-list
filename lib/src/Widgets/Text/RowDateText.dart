import 'package:flutter/material.dart';

class RowDateText extends StatelessWidget {
  final String _date;

  RowDateText(this._date);

  @override
  Widget build(BuildContext context) {
    if (this._date == null) {
      return null;
    }

    List<String> date = this._date.split('.');

    return Column(
      children: <Widget>[
        this._mainText(date[0]),
        this._subText("${date[1]}.${date[2]}"),
      ],
    );
  }

  Widget _mainText(String text) => Stack(
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              fontSize: 25,
              letterSpacing: 1.0,
              foreground: Paint()
                ..style = PaintingStyle.stroke
                ..strokeWidth = 6
                ..color = Colors.blue[700],
            ),
          ),
          Text(
            text,
            style: TextStyle(
              letterSpacing: 1.0,
              fontSize: 25,
              color: Colors.grey[300],
            ),
          ),
        ],
      );

  Widget _subText(String text) => Padding(
        padding: const EdgeInsets.only(top: 3.0),
        child: Text(
          text,
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.black.withOpacity(0.6),
          ),
        ),
      );
}
