import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo/src/Widgets/Cards/NoteCard.dart';

class DismissibleCard extends StatelessWidget {
  final String _title;
  final String _subTitle;
  final Widget _listLeading;
  final Function _onTap;
  final Color _leftColor;
  final Color _rightColor;
  final Widget _leftWidget;
  final Widget _rightWidget;
  final Function _leftDirection;
  final Function _rightDirection;

  DismissibleCard(
      {Key key,
      String title,
      String subTitle,
      Widget listLeading,
      Function onTap,
      Color leftColor,
      Color rightColor,
      Widget leftWidget,
      Widget rightWidget,
      Function leftDirection,
      Function rightDirection})
      : this._title = title,
        this._subTitle = subTitle,
        this._listLeading = listLeading,
        this._onTap = onTap,
        this._leftColor = leftColor,
        this._rightColor = rightColor,
        this._leftWidget = leftWidget,
        this._rightWidget = rightWidget,
        this._leftDirection = leftDirection,
        this._rightDirection = rightDirection,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, Widget> background = _getBackground();

    return Dismissible(
      key: key,
      direction: _getDirection(),
      onDismissed: _onDisMiss,
      child: _renderCard(),
      background: background['main'],
      secondaryBackground: background['sub'],
    );
  }

  _onDisMiss(DismissDirection direction) {
    switch (direction) {
      case DismissDirection.startToEnd:
        _rightDirection();
        break;
      case DismissDirection.endToStart:
        _leftDirection();
        break;
      default:
        break;
    }
  }

  Widget _renderCard() =>
      NoteCard(
        key: key,
        title: _title,
        subTitle: _subTitle,
        listLeading: _listLeading,
        onTap: _onTap,
      );

  DismissDirection _getDirection() {
    if (_rightDirection != null && _leftDirection != null) {
      return DismissDirection.horizontal;
    }

    if (_rightDirection != null) {
      return DismissDirection.startToEnd;
    }

    if (_leftDirection != null) {
      return DismissDirection.endToStart;
    }

    return null;
  }

  Map<String, Widget> _getBackground() {
    if (_leftWidget != null && _rightWidget != null) {
      return {"main": _rightWidget, "sub": _leftWidget};
    }

    if (_rightWidget != null) {
      return {
        "main": _rightWidget,
        "sub": _leftColor != null ? _getColorContainer(_leftColor) : null,
      };
    }

    if (_leftWidget != null) {
      return {
        "main": _leftWidget,
        "sub": _rightColor != null ? _getColorContainer(_rightColor) : null,
      };
    }

    if (_rightColor != null && _leftColor != null) {
      return {
        "main": _getColorContainer(_rightColor),
        "sub": _getColorContainer(_leftColor),
      };
    }

    if (_rightColor != null) {
      return {
        "main": _getColorContainer(_rightColor),
        "sub": null,
      };
    }

    if (_leftColor != null) {
      return {
        "main": _getColorContainer(_leftColor),
        "sub": null,
      };
    }

    return {"main": null, "sub": null};
  }

  Widget _getColorContainer(Color color) => Container(color: color);
}
