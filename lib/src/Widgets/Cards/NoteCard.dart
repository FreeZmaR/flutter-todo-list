import 'package:flutter/material.dart';

class NoteCard extends StatelessWidget {
  final String _title;
  final String _subTitle;
  final Widget _listLeading;
  final Function _onTap;

  NoteCard(
      {Key key,
      String title,
      String subTitle,
      Widget listLeading,
      Function onTap})
      : this._title = title,
        this._subTitle = subTitle,
        this._listLeading = listLeading,
        this._onTap = onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 1.0),
        child: ListTile(
          subtitle: this._subTitleWidget(),
          title: Center(child: Text(this._title)),
          onTap: () => this._onTap(),
          leading: this._listLeading ?? null,
        ),
      ),
    );
  }

  Widget _subTitleWidget() => Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: Text(
          this._subTitle ?? '',
          maxLines: 1,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Colors.black.withOpacity(0.4),
          ),
        ),
      );
}
