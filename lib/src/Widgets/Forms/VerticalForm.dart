import 'package:flutter/material.dart';
import 'package:todo/src/Core/Helpers/Collection.dart';

class VerticalForm extends StatelessWidget {
  List<Widget> _inputs = [];
  Map<String, TextEditingController> _controllers =
      <String, TextEditingController>{};

  VerticalForm({List inputs}) {
    for (Map input in inputs) {
      _controllers[input['name']] = TextEditingController();
      _inputs.add(_getInput(input));
    }
  }

  Map<String, String> get formData {
    Map<String, String> _formData = <String, String>{};
    for (var controller in _controllers.entries) {
      _formData[controller.key] = controller.value.text;
    }

    return _formData;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: _inputs,
    );
  }

  Widget _getInput(input) => Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: TextField(
          maxLines: Collection.getDefault(input, 'lines', defaultValue: 1),
          controller: _controllers[input['name']]
            ..text = Collection.getDefault(input, 'value'),
          decoration: InputDecoration(
            hintText: Collection.getDefault(input, 'placeholder'),
            hintStyle: TextStyle(
              color: Colors.black.withOpacity(0.5),
            ),
            fillColor: Colors.white,
            filled: true,
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blueAccent),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide.none,
            ),
          ),
        ),
      );
}
