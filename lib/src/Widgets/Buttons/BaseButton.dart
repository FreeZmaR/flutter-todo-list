import 'package:flutter/material.dart';
import 'package:todo/src/Styles/Colors/BootstrapColors.dart';

class BaseButton extends StatelessWidget {
  final String title;
  final String buttonClass;
  final Function onPress;
  final double paddingTop;
  final double paddingBottom;
  final double paddingLeft;
  final double paddingRight;

  BaseButton(this.title,
      {this.buttonClass,
      this.onPress,
      this.paddingLeft,
      this.paddingRight,
      this.paddingBottom,
      this.paddingTop});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: this.paddingTop ?? 0.0,
          bottom: this.paddingBottom ?? 0.0,
          left: this.paddingLeft ?? 0.0,
          right: this.paddingRight ?? 0.0),
      child: FlatButton(
        color: BootstrapColors.getBackgroundColor(buttonClass),
        textColor: BootstrapColors.getTextColor(buttonClass),
        disabledColor:
            BootstrapColors.getBackgroundColor(buttonClass, opacity: 0.6),
        disabledTextColor:
            BootstrapColors.getTextColor(buttonClass, opacity: 0.6),
        padding: EdgeInsets.all(4.0),
        splashColor: BootstrapColors.getBackgroundHoverColor(buttonClass),
        onPressed: () {
          onPress();
        },
        child: Text(
          this.title,
          style: TextStyle(fontSize: 14.0),
        ),
      ),
    );
  }
}
