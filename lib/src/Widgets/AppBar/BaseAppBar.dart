import 'package:flutter/material.dart';

class BaseAppBar {
  String title = '';
  List<Widget> actions;

  PreferredSizeWidget get base => AppBar(
        title: Text(title),
        centerTitle: true,
        backgroundColor: Colors.blue,
        actions: actions,
      );
}
