import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:todo/src/Actions/TodoActions.dart';
import 'package:todo/src/State/Models/AppState.dart';
import 'package:todo/src/State/Models/Todo/NoteModel.dart';
import 'package:todo/src/Widgets/AppBar/BaseAppBar.dart';
import 'package:todo/src/Widgets/Cards/DismissibleCard.dart';
import 'package:todo/src/Widgets/Text/RowDateText.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BaseAppBar _appBar = BaseAppBar()
      ..title = 'Home'
      ..actions = <Widget>[
        IconButton(
          icon: Icon(Icons.create),
          onPressed: () => _onPressCreate(context),
        ),
      ];

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: _appBar.base,
      body: Container(
        child: this._getList(),
      ),
    );
  }

  _onPressCreate(context) {
    Navigator.pushNamed(context, 'Note').then((data) {
      if (data == null) {
        return;
      }

      TodoActions.addNote(context, data);
    });
  }

  _onPressNote(context, NoteModel data) {
    Navigator.pushNamed(context, 'Note', arguments: {
      'id': data.id,
      'title': data.title,
      'text': data.text,
      'createDate': data.createDate
    }).then((newData) {
      if (newData == null) {
        return;
      }

      TodoActions.updateNote(context, newData, data.id);
    });
  }

  _onDismissRight(context, String id) =>
      TodoActions.updateNote(context, {'expire': true}, id);

  _onDismissLeft(context, String id) => TodoActions.deleteNote(context, id);

  Widget _getList() => StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          if (!state.todo.isLoading && !state.todo.isLoaded) {
            TodoActions.loadNoteFromLocalStore(context);
          }

          return ListView.builder(
            itemCount: state.todo.notes?.length ?? 0,
            itemBuilder: (context, index) =>
                this._renderItem(context, state.todo.notes[index]),
          );
        },
      );

  Widget _renderItem(context, NoteModel data) {
    String showDate =
        data.updateDate != null ? data.updateDate : data.createDate;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
      child: DismissibleCard(
        key: UniqueKey(),
        title: data.title,
        subTitle: data.text,
        listLeading: RowDateText(showDate),
        onTap: () => this._onPressNote(context, data),
        rightDirection: () => _onDismissRight(context, data.id),
        leftDirection: () => _onDismissLeft(context, data.id),
        rightWidget: Container(
          color: Colors.green,
          child: Text('Active'),
        ),
        leftWidget: Container(
          color: Colors.red,
          child: Text('Delete'),
        ),
      ),
    );
  }
}
