import 'package:flutter/material.dart';
import 'package:todo/src/Actions/TodoActions.dart';
import 'package:todo/src/Core/Helpers/Collection.dart';
import 'package:todo/src/Widgets/AppBar/BaseAppBar.dart';
import 'package:todo/src/Widgets/Buttons/BaseButton.dart';
import 'package:todo/src/Widgets/Forms/VerticalForm.dart';

class NotePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments;
    bool creating = true;
    String _title = 'Note';
    List<Widget> headerButtons = <Widget>[];

    if (args != null) {
      creating = false;
      _title = Collection.getDefault(args, 'title', defaultValue: _title);
      headerButtons.add(IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.red,
        ),
        onPressed: () =>
            _onPressDelete(context, Collection.getDefault(args, 'id')),
      ));
    }

    BaseAppBar _appBar = BaseAppBar()
      ..title = _title
      ..actions = headerButtons;

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: _appBar.base,
      body: this._getBody(
        context,
        text: Collection.getDefault(args, 'text'),
        title: Collection.getDefault(args, 'title'),
        date: Collection.getDefault(args, 'createDate'),
        creating: creating,
      ),
    );
  }

  _onPressedCreateUpdate(context, Map<String, String> data) =>
      Navigator.pop(context, data);

  _onPressDelete(context, String id) {
    TodoActions.deleteNote(context, id);
    Navigator.pop(context);
  }

  _onPressCancelCreateUpdate(context) => Navigator.pop(context);

  Widget _getBody(context,
      {String title, String text, String date, bool creating = true}) {
    VerticalForm form = VerticalForm(
      inputs: [
        {
          'name': 'title',
          'value': title,
          'placeholder': 'Title',
        },
        {
          'name': 'text',
          'value': text,
          'lines': 6,
          'placeholder': 'Input note text her ...',
        },
      ],
    );

    return Container(
      padding: const EdgeInsets.only(top: 8, left: 5, right: 5),
      child: Column(
        children: <Widget>[
          form,
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                BaseButton(
                  creating ? 'Create' : 'Save',
                  buttonClass: creating ? 'primary' : 'warning',
                  paddingRight: 10,
                  onPress: () => _onPressedCreateUpdate(
                      context, {...form.formData, "createDate": date}),
                ),
                BaseButton(
                  'cancel',
                  buttonClass: 'secondary',
                  onPress: () => _onPressCancelCreateUpdate(context),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
