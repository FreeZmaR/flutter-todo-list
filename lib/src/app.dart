import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:todo/src/State/Models/AppState.dart';

import 'Routes/AppRoutes.dart';
import 'Routes/Home/HomeRoute.dart';

class App extends StatelessWidget {
  final Store<AppState> store;

  const App({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: this.store,
      child: MaterialApp(
        home: HomeRoute().homePage,
        routes: AppRoutes().routing,
      ),
    );
  }
}
