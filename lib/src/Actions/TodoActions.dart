import 'package:flutter_redux/flutter_redux.dart';
import 'package:todo/src/Core/Helpers/Collection.dart';
import 'package:todo/src/Core/Store/Local/SharedPreferencesStore.dart';
import 'package:todo/src/State/Actions/TodoStateActions.dart';
import 'package:todo/src/State/Models/AppState.dart';
import 'package:todo/src/State/Models/Todo/NoteModel.dart';
import 'package:todo/src/State/Selectors/selectors.dart';

class TodoActions {
  static void loadNoteFromLocalStore(context) {
    StoreProvider.of<AppState>(context).dispatch(TodoStateLoadingAction(true));
    SharedPreferencesStore.get('notes', decode: true).then((data) {
      StoreProvider.of<AppState>(context).dispatch(TodoStateLoadedAction(true));

      if (data == null) {
        StoreProvider.of<AppState>(context)
            .dispatch(TodoStateLoadingAction(false));
        return;
      }

      StoreProvider.of<AppState>(context).dispatch(TodoStateNoteAddListAction(
          List<Map<String, Object>>.from(data).toList()));

      StoreProvider.of<AppState>(context)
          .dispatch(TodoStateLoadingAction(false));
    });
  }

  static void addNote(context, Map<String, Object> noteData) {
    if (noteData == null) {
      return;
    }

    StoreProvider.of<AppState>(context).dispatch(TodoStateLoadingAction(true));
    StoreProvider.of<AppState>(context).dispatch(TodoStateNoteAddAction(
        NoteModel(title: noteData['title'], text: noteData['text'])));

    TodoActions.saveNotesToLocalStorage(context);
  }

  static void updateNote(context, Map<String, Object> noteData, String noteId) {
    if (noteData == null) {
      return;
    }

    StoreProvider.of<AppState>(context).dispatch(TodoStateLoadingAction(true));

    NoteModel note = TodoSelect.getNote(context, noteId);
    NoteModel newNote = note.fromEntity(
      newExpire: Collection.getDefault(noteData, 'expire'),
      newTitle: Collection.getDefault(noteData, 'title'),
      newText: Collection.getDefault(noteData, 'text'),
    );
    StoreProvider.of<AppState>(context)
        .dispatch(TodoStateNoteUpdateAction(noteId, newNote));

    TodoActions.saveNotesToLocalStorage(context);
  }

  static void deleteNote(context, String id) {
    StoreProvider.of<AppState>(context).dispatch(TodoStateLoadingAction(true));
    StoreProvider.of<AppState>(context).dispatch(TodoStateNoteDeleteAction(id));
    StoreProvider.of<AppState>(context).dispatch(TodoStateLoadingAction(false));
  }

  static void saveNotesToLocalStorage(context) {
    List<Map<String, Object>> notes = TodoSelect.getNoteListToJson(context);

    // ignore: unnecessary_statements
    SharedPreferencesStore.save('notes', notes).whenComplete(() =>
        StoreProvider.of<AppState>(context).dispatch(
            StoreProvider.of<AppState>(context)
                .dispatch(TodoStateLoadingAction(false))));
  }
}
