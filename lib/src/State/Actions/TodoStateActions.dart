import 'package:todo/src/State/Models/Todo/NoteModel.dart';

class TodoInitAction {}

class TodoStateLoadingAction {
  final bool isLoading;
  TodoStateLoadingAction(this.isLoading);

  static bool action(bool state, TodoStateLoadingAction action) =>
      action.isLoading;
}

class TodoStateLoadedAction {
  final bool isLoaded;
  TodoStateLoadedAction(this.isLoaded);

  static bool action(bool state, TodoStateLoadedAction action) => true;
}

class TodoStateNoteAddAction {
  final NoteModel note;
  TodoStateNoteAddAction(this.note);

  static List<NoteModel> action(
          List<NoteModel> state, TodoStateNoteAddAction action) =>
      List.from(state)..add(action.note);
}

class TodoStateNoteUpdateAction {
  final String id;
  final NoteModel note;
  TodoStateNoteUpdateAction(this.id, this.note);

  static List<NoteModel> action(
          List<NoteModel> state, TodoStateNoteUpdateAction action) =>
      state
          .map((element) => element.id == action.id ? action.note : element)
          .toList();
}

class TodoStateNoteDeleteAction {
  final String id;
  TodoStateNoteDeleteAction(this.id);

  static List<NoteModel> action(
          List<NoteModel> state, TodoStateNoteDeleteAction action) =>
      state.where((element) => element.id != action.id).toList();
}

class TodoStateNoteAddListAction {
  final List<Map<String, Object>> notes;
  TodoStateNoteAddListAction(this.notes);

  static List<NoteModel> action(
          List<NoteModel> state, TodoStateNoteAddListAction action) =>
      action.notes.map((note) => NoteModel.fromJson(note)).toList();
}
