import 'package:todo/src/Core/Store/GlobalState/Model/BaseStateModel.dart';

import 'Todo/TodoState.dart';

class AppState implements BaseStateModel {
  final bool isLoading;
  final TodoState todo;

  AppState({
    todo,
    this.isLoading = false,
  }) : this.todo = todo != null ? todo : TodoState();

  @override
  Map<String, Object> get props => {"isLoading": isLoading, "todo": todo};

  AppState copyWith(bool isLoading, TodoState todo) =>
      AppState(isLoading: isLoading ?? this.isLoading, todo: todo ?? this.todo);

  @override
  String toString() {
    return "IsLoading: $isLoading \n Todo: $todo \n";
  }
}
