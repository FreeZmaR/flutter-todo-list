import 'package:todo/src/Core/Helpers/Date.dart';
import 'package:todo/src/Core/Helpers/Uuid.dart';
import 'package:todo/src/Core/Store/GlobalState/Model/BaseStateModel.dart';

class NoteModel extends BaseStateModel {
  final String id;
  final bool expire;
  final String title;
  final String text;
  final String createDate;
  final String updateDate;

  NoteModel(
      {String id,
      String createDate,
      this.expire = false,
      this.title = '',
      this.text = '',
      this.updateDate})
      : this.id = id ?? Uuid().generate(),
        this.createDate = createDate != null ? createDate : Date.dateNow();

  NoteModel fromEntity({bool newExpire, String newTitle, String newText}) =>
      NoteModel(
        id: this.id,
        expire: newExpire ?? this.expire,
        title: newTitle ?? this.title,
        text: newText ?? this.text,
        createDate: this.createDate,
        updateDate: Date.dateNow(),
      );

  @override
  Map<String, Object> get props => {
        "id": this.id,
        "expire": this.expire,
        "title": this.title,
        "text": this.text,
        "createDate": this.createDate,
        "updateDate": this.updateDate,
      };

  Map<String, Object> toJson() => {
        "id": this.id,
        "expire": this.expire,
        "title": this.title,
        "text": this.text,
        "createDate": this.createDate,
        "updateDate": this.updateDate,
      };

  static NoteModel fromJson(Map<String, Object> json) => NoteModel(
        id: json['id'] as String,
        expire: json['expire'] as bool,
        title: json['title'] as String,
        text: json['text'] as String,
        createDate: json['createDate'] as String,
        updateDate:
            json['updateDate'] != '' ? json['updateDate'] as String : null,
      );
}
