import 'package:todo/src/Core/Store/GlobalState/Model/BaseStateModel.dart';

import 'NoteModel.dart';

class TodoState implements BaseStateModel {
  final bool isLoading;
  final bool isLoaded;
  final List<NoteModel> notes;

  TodoState(
      {this.isLoading = false,
      this.isLoaded = false,
      this.notes = const <NoteModel>[]});

  @override
  Map<String, Object> get props => {"isLoading": isLoading, "notes": notes};

  List<Map<String, Object>> notesToJson() =>
      notes.map((note) => note.toJson()).toList();
}
