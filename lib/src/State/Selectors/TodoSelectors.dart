import 'package:flutter_redux/flutter_redux.dart';
import 'package:todo/src/State/Models/AppState.dart';
import 'package:todo/src/State/Models/Todo/NoteModel.dart';

class TodoSelect {
  static NoteModel getNote(context, String id) =>
      StoreProvider.of<AppState>(context)
          .state
          .todo
          .notes
          .firstWhere((note) => note.id == id);

  static List<Map<String, Object>> getNoteListToJson(context) =>
      StoreProvider.of<AppState>(context).state.todo.notesToJson();
}
