import 'package:redux/redux.dart';
import 'package:todo/src/State/Actions/TodoStateActions.dart';
import 'package:todo/src/State/Models/Todo/NoteModel.dart';
import 'package:todo/src/State/Models/Todo/TodoState.dart';

TodoState initTodoReducer(TodoState state, action) => TodoState(
      isLoading: todoLoadingReducer(state.isLoading, action),
      isLoaded: todoLoadedReducer(state.isLoaded, action),
      notes: todoNoteReducer(state.notes, action),
    );

final todoLoadingReducer = combineReducers<bool>([
  TypedReducer<bool, TodoStateLoadingAction>(TodoStateLoadingAction.action),
]);

final todoLoadedReducer = combineReducers<bool>([
  TypedReducer<bool, TodoStateLoadedAction>(TodoStateLoadedAction.action),
]);

final todoNoteReducer = combineReducers<List<NoteModel>>([
  TypedReducer<List<NoteModel>, TodoStateNoteAddAction>(
      TodoStateNoteAddAction.action),
  TypedReducer<List<NoteModel>, TodoStateNoteUpdateAction>(
      TodoStateNoteUpdateAction.action),
  TypedReducer<List<NoteModel>, TodoStateNoteDeleteAction>(
      TodoStateNoteDeleteAction.action),
  TypedReducer<List<NoteModel>, TodoStateNoteAddListAction>(
      TodoStateNoteAddListAction.action),
]);
