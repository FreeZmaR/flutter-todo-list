import 'package:redux/redux.dart';
import 'package:todo/src/State/Actions/AppStateActions.dart';
import 'package:todo/src/State/Models/AppState.dart';
import 'package:todo/src/State/Reducers/Todo/todoReducer.dart';

AppState appReducer(AppState state, action) => AppState(
      isLoading: loadingAppReducer(state.isLoading, action),
      todo: initTodoReducer(state.todo, action),
    );

final loadingAppReducer = combineReducers<bool>([
  TypedReducer<bool, AppStateLoadingAction>(AppStateLoadingAction.action),
]);
