import 'package:flutter/material.dart';

class BootstrapColors {
  BootstrapColors._();

  ///  ===== Bootstrap colors for background and text ====
  ///
  /// ======= Background ======
  static const Color primary = Color(0x007bff);
  static const Color secondary = Color(0x6c757d);
  static const Color success = Color(0x28a745);
  static const Color danger = Color(0xdc3545);
  static const Color warning = Color(0xffc107);
  static const Color info = Color(0x17a2b8);
  static const Color light = Color(0xf8f9fa);
  static const Color dark = Color(0x343a40);

  /// ===== Background Hover ======
  static const Color primaryHover = Color(0x0069d9);
  static const Color secondaryHover = Color(0x5a6268);
  static const Color successHover = Color(0x218838);
  static const Color dangerHover = Color(0xc82333);
  static const Color warningHover = Color(0xe0a800);
  static const Color infoHover = Color(0x138496);
  static const Color lightHover = Color(0xe2e6ea);
  static const Color darkHover = Color(0x23272b);

  /// ====== Text ====
  static const Color textBlack = Color(0x212529);
  static const Color textWhite = Color(0xffffff);

  /// Get color from class name by bootstrap class
  /// Default value [primary]
  static Color getBackgroundColor(String bootstrapClass,
      {double opacity = 1.0}) {
    Color selected;
    switch (bootstrapClass) {
      case "primary":
        selected = primary;
        break;
      case "secondary":
        selected = secondary;
        break;
      case "success":
        selected = success;
        break;
      case "danger":
        selected = danger;
        break;
      case "warning":
        selected = warning;
        break;
      case "info":
        selected = info;
        break;
      case "light":
        selected = light;
        break;
      case "dark":
        selected = dark;
        break;
      default:
        selected = primary;
    }

    return selected.withOpacity(opacity);
  }

  /// Get color from class name by bootstrap class
  /// Default value [primaryHover]
  static Color getBackgroundHoverColor(String bootstrapClass,
      {double opacity = 1.0}) {
    Color selected;
    switch (bootstrapClass) {
      case "primary":
        selected = primaryHover;
        break;
      case "secondary":
        selected = secondaryHover;
        break;
      case "success":
        selected = successHover;
        break;
      case "danger":
        selected = dangerHover;
        break;
      case "warning":
        selected = warningHover;
        break;
      case "info":
        selected = infoHover;
        break;
      case "light":
        selected = lightHover;
        break;
      case "dark":
        selected = darkHover;
        break;
      default:
        selected = primaryHover;
    }

    return selected.withOpacity(opacity);
  }

  /// Get color from text by name bootstrap class
  /// Default value [textBlack]
  static Color getTextColor(String bootstrapClass, {double opacity = 1.0}) {
    Color selected;
    switch (bootstrapClass) {
      case "primary":
        selected = textWhite;
        break;
      case "secondary":
        selected = textWhite;
        break;
      case "success":
        selected = textWhite;
        break;
      case "danger":
        selected = textWhite;
        break;
      case "warning":
        selected = textBlack;
        break;
      case "info":
        selected = textWhite;
        break;
      case "light":
        selected = textBlack;
        break;
      case "dark":
        selected = textWhite;
        break;
      default:
        selected = textBlack;
    }

    return selected.withOpacity(opacity);
  }
}
